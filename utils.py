import subprocess
from typing import List
from google.cloud import compute_v1
from gcp_sdk_snippets import disk_from_image
from google.oauth2 import service_account


def get_vm_disks(creds:service_account.Credentials, zone:str, local_storage_gb:int=100)->List[compute_v1.AttachedDisk]:
    # found via $ gcloud compute images list --filter ubuntu
    BOOT_DISK_PROJECT, BOOT_DISK_NAME = "ubuntu-os-cloud", "ubuntu-2004-focal-v20220419"

    ssd_disk = f"zones/{zone}/diskTypes/pd-ssd"
    boot_disk = disk_from_image(disk_type = ssd_disk, 
        disk_size_gb = None,  # changed to None to avoid "'Disk size: '30 GB' is larger than image size: '10 GB'."
        boot = True,
        source_image = f"projects/{BOOT_DISK_PROJECT}/global/images/{BOOT_DISK_NAME}",
        auto_delete = True)

    local_storage = disk_from_image(disk_type=ssd_disk,
        disk_size_gb = local_storage_gb,
        boot = False,
        auto_delete=True,
        source_image = None)  # non-bootable disks don't have source-image, but got error w/o including param at all

    return (boot_disk, local_storage)
import os
import typer
import subprocess
from rich.console import Console
from utils import get_vm_disks
from gcp_sdk_snippets import create_instance, disk_from_image
from google.cloud import compute_v1
from google.oauth2 import service_account
from datetime import datetime

class sdtException(Exception):
    pass

app = typer.Typer()




def deploy(repo:str = typer.Option(..., prompt=True), 
           cmd: str = typer.Option(..., prompt=True),
           vmtype: str = typer.Option('e2-standard-4', prompt=True),
           region: str = typer.Option('us-central1', prompt=True),
           path_to_svc_acct = typer.Option("creds/dsops-testbed-c6df08f82c16.json", prompt=True)
           ):
    # derived constants
    ZONE = f"{region}-a"
    
    # E-2 Machine pricing on-demand is $0.02/vCPU-hour + $0.003/GB-hour
    typer.echo(f"deploying {cmd} from {repo} on {vmtype}")

    # get any relevant project params from service account
    path_to_svc_acct = "creds/dsops-testbed-c6df08f82c16.json"
    creds = service_account.Credentials.from_service_account_file(path_to_svc_acct)

    # spin up vm
    boot_disk, local_storage = get_vm_disks(creds, zone=ZONE, local_storage_gb=100)

    timestamp = datetime.utcnow().strftime("%Y-%m-%d-%H-%M")
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = path_to_svc_acct
    job_script = f"""git clone {repo} -y
    """
    
    try:
        # note: list available gcp public images with `$ gcloud compute images list filter ubuntu`. We use 20.04LTS
        vm = create_instance(creds = creds,
        zone = ZONE,
        instance_name = f"sdt-vm-{timestamp}",
        job_script = job_script,
        disks = [boot_disk, local_storage],
        machine_type = vmtype,
        accelerators = None,
        preemptible = False)
    except Exception as e:
        raise sdtException(f"sdt failed to start a vm, threw:\n{e}")
    print("finished")

    return None

if __name__ == "__main__":
    typer.run(deploy)
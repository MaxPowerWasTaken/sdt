import subprocess

def parse_config_region_results(stdout):
    l1 = stdout.split('\n')
    l2 = [i for i in l1 if "=" in i]

    # we now have a list of config params of form [k1 = v1, k2 = v2, ...]
    d = {}
    for i in l2:
        k,v = i.split("=")
        d[k.strip()] = v.strip()
    return d['region']


def get_default_compute_region():
    cmd = "gcloud config list compute/region"
    result = subprocess.run(cmd, capture_output=True, text=True, shell=True)
    if result.returncode == 0:
        region = parse_config_region_results(result.stdout)
    else:
        print(f"cmd: '{cmd}' returned non-zero error-code")
        raise 

    return region
